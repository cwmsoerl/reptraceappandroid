package reptrace.reptraceappandroid;

import android.content.Intent;
import android.content.SharedPreferences;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.Switch;
import android.net.Uri;

public class SettingsActivity extends AppCompatActivity {

    private static final String LOG_TAG = SettingsActivity.class.getSimpleName();

    private Switch switchThemeButton;
    private static final String PREFS_NAME = "prefs";
    private static final String PREF_DARK_THEME = "dark_theme";


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        SharedPreferences preferences = getSharedPreferences(PREFS_NAME, MODE_PRIVATE);
        boolean useDarkTheme = preferences.getBoolean(PREF_DARK_THEME, false);

        if(useDarkTheme) {
            setTheme(R.style.AppTheme_Dark);
        }
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_settings);

        Switch toggle = (Switch) findViewById(R.id.switchThemeSwitchSettings);
        toggle.setChecked(useDarkTheme);
        toggle.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton view, boolean isChecked) {
                toggleTheme(isChecked);
            }
        });

        Button startBtn = (Button) findViewById(R.id.imageViewCallUs);
        startBtn.setOnClickListener(new View.OnClickListener() {

            public void onClick(View view) {
                String number = "+31650272050";
                Intent i=new Intent(Intent.ACTION_DIAL,Uri.parse("tel:" + number));
                startActivity(i);
            }
        });

        Button callButton = (Button) findViewById(R.id.imageViewCallUs);
        callButton.setOnClickListener(new View.OnClickListener() {

            public void onClick(View view) {
                String number = "+31650272050";
                Intent i=new Intent(Intent.ACTION_DIAL,Uri.parse("tel:" + number));
                startActivity(i);
            }
        });

        Button emailButton = (Button) findViewById(R.id.imageViewEmailUs);
        emailButton.setOnClickListener(new View.OnClickListener() {

            public void onClick(View view) {
                Intent intent = new Intent(Intent.ACTION_SEND);
                intent.setType("plain/text");
                intent.putExtra(Intent.EXTRA_EMAIL, new String[] { "info@reptrace.com" });
                intent.putExtra(Intent.EXTRA_SUBJECT, "subject");
                intent.putExtra(Intent.EXTRA_TEXT, "Email content");
                startActivity(Intent.createChooser(intent, "Request for Reptrace info"));
            }
        });

    }

    private void toggleTheme(boolean darkTheme) {
        SharedPreferences.Editor editor = getSharedPreferences(PREFS_NAME, MODE_PRIVATE).edit();
        editor.putBoolean(PREF_DARK_THEME, darkTheme);
        editor.apply();

        Intent intent = getIntent();
        finish();

        startActivity(intent);
    }

    public void MainActivity(View view) {
        Log.d(LOG_TAG, "Close Button Settings clicked!");
        Intent mainActivity = new Intent(this, MainActivity.class);
        startActivity(mainActivity);
    }

    public void onClick(View view) {
    }
}
