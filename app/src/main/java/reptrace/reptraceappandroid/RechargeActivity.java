package reptrace.reptraceappandroid;

import android.content.Intent;
import android.content.SharedPreferences;
import android.provider.Settings;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.webkit.GeolocationPermissions;
import android.webkit.WebChromeClient;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.CompoundButton;
import android.widget.Switch;

public class RechargeActivity extends AppCompatActivity {

    private static final String LOG_TAG = RechargeActivity.class.getSimpleName();

    private Switch switchThemeButton;
    private static final String PREFS_NAME = "prefs";
    private static final String PREF_DARK_THEME = "dark_theme";
    private WebView webViewRechargeStations;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        SharedPreferences preferences = getSharedPreferences(PREFS_NAME, MODE_PRIVATE);
        boolean useDarkTheme = preferences.getBoolean(PREF_DARK_THEME, false);

        if(useDarkTheme) {
            setTheme(R.style.AppTheme_Dark);
        }
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_recharge);

        Switch toggle = (Switch) findViewById(R.id.switchThemeSwitchRecharge);
        toggle.setVisibility(View.INVISIBLE);
        toggle.setChecked(useDarkTheme);
        toggle.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton view, boolean isChecked) {
                toggleThemeMain(isChecked);
            }
        });

        webViewRechargeStations = (WebView) findViewById(R.id.webviewRechargeStations);
        WebSettings webSettings = webViewRechargeStations.getSettings();
        webSettings.setJavaScriptEnabled(true);
        webSettings.setGeolocationEnabled(true);
        webSettings.setGeolocationDatabasePath( this.getFilesDir().getPath() );
        webSettings.setAppCacheEnabled(true);
        webSettings.setDatabaseEnabled(true);
        webSettings.setDomStorageEnabled(true);
        webViewRechargeStations.setWebViewClient(new WebViewClient());
        webViewRechargeStations.loadUrl("https://www.plugshare.com/widget2.html?latitude=52.132633&longitude=5.291265999999999&spanLat=0.02&spanLng=0.02&plugs=2,3,4,5,6,42,13,7,8,9,10,12,14,15' width='800' height='600' allow='geolocation/");
        webViewRechargeStations.setWebChromeClient(new WebChromeClient() {
            public void onGeolocationPermissionsShowPrompt(String origin, GeolocationPermissions.Callback callback) {
                callback.invoke(origin, true, true);

                Intent intent = new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS);
                startActivity(intent);
            }
        });

    }

    private void toggleThemeMain(boolean darkTheme) {
        SharedPreferences.Editor editor = getSharedPreferences(PREFS_NAME, MODE_PRIVATE).edit();
        editor.putBoolean(PREF_DARK_THEME, darkTheme);
        editor.apply();

        Intent intent = getIntent();
        finish();

        startActivity(intent);
    }

    public void MainActivity(View view) {
        Log.d(LOG_TAG, "Close Button Recharge clicked!");
        Intent mainActivity = new Intent(this, MainActivity.class);
        startActivity(mainActivity);
    }
}
