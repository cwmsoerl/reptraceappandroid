package reptrace.reptraceappandroid;

import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.support.v4.view.MenuItemCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.SearchView;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.Switch;
import android.widget.Toast;
import java.util.ArrayList;

import Adapters.VehicleAdapter;
import Controllers.DatabaseHelper;
import Models.Vehicles;


public class VehiclesActivity extends AppCompatActivity {

    private static final String LOG_TAG = SettingsActivity.class.getSimpleName();
    private Switch switchThemeButton;
    private static final String PREFS_NAME = "prefs";
    private static final String PREF_DARK_THEME = "dark_theme";

    DatabaseHelper db;
    ListView vehiclesList;

    ArrayList<Vehicles> listItem = new ArrayList<>();
    VehicleAdapter adapter;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        SharedPreferences preferences = getSharedPreferences(PREFS_NAME, MODE_PRIVATE);
        boolean useDarkTheme = preferences.getBoolean(PREF_DARK_THEME, false);

        if(useDarkTheme) {
            setTheme(R.style.AppTheme_Dark);
        }
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_vehicles);

        Switch toggle = (Switch) findViewById(R.id.switchThemeSwitchVehicles);
        toggle.setVisibility(View.INVISIBLE);
        toggle.setChecked(useDarkTheme);
        toggle.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton view, boolean isChecked) {
                toggleThemeVehicles(isChecked);
            }
        });

        db = new DatabaseHelper(this);
        vehiclesList = findViewById(R.id.vehiclesListView);


        showVehicleData();

        vehiclesList.setOnItemClickListener(new AdapterView.OnItemClickListener(){
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                final Vehicles vehicle = listItem.get(position);
                if (vehicle != null) {
                    Intent intent = new Intent(VehiclesActivity.this, VehicleUpdateActivity.class);
                    intent.putExtra("vehicle", vehicle);
                    startActivity(intent);
                }
//
//                listItem = db.getAllVehicles();
//
//                Vehicles vehicle = listItem.get(position);
//
//                Intent intent = new Intent(VehiclesActivity.this, VehicleUpdateActivity.class);
//                intent.putExtra(, vehicle);
//                startActivity(intent);
            }
        });

    }

    private void toggleThemeVehicles(boolean darkTheme) {
        SharedPreferences.Editor editor = getSharedPreferences(PREFS_NAME, MODE_PRIVATE).edit();
        editor.putBoolean(PREF_DARK_THEME, darkTheme);
        editor.apply();

        Intent intent = getIntent();
        finish();

        startActivity(intent);
    }

    private void showVehicleData() {

        listItem = db.getAllVehicles();
        adapter = new VehicleAdapter(this, listItem);
        if(adapter.getCount() == 0){

            Toast.makeText(VehiclesActivity.this, "No vehicles available", Toast.LENGTH_SHORT).show();

        } else {

            vehiclesList.setAdapter(adapter);
            adapter.notifyDataSetChanged();
        }


    }

    /* public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater searchInflater = getMenuInflater();
        searchInflater.inflate(R.menu.menu, menu);

        MenuItem searchVehicle = menu.findItem(R.id.menu_search);
        SearchView searchView = (SearchView) MenuItemCompat.getActionView(searchVehicle);

        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                return false;
            }

            @Override
            public boolean onQueryTextChange(String newText) {

                ArrayList<Vehicles> vehicleList = new ArrayList<>();

                for (Vehicles vehicle : listItem) {
                    if (vehicle.toLowerCase().contains(newText.toLowerCase())) {
                        vehicleList.add(vehicle);
                    }
                }

                VehicleAdapter adapter = new VehicleAdapter(VehiclesActivity.this,vehicleList);
                vehiclesList.setAdapter(adapter);

                return true;
            }
        });

        return super.onCreateOptionsMenu(menu);
    } */


    public void MainActivity(View view) {
        Log.d(LOG_TAG, "Close Button Vehicles clicked!");
        Intent mainActivity = new Intent(this, MainActivity.class);
        startActivity(mainActivity);
    }

    public void AddVehiclesActivity(View view) {
        Log.d(LOG_TAG, "Add Button Vehicles clicked!");
        Intent addVehiclesActivity = new Intent(this, AddVehicleActivity.class);
        startActivity(addVehiclesActivity);
    }

    public void VehiclesUpdateActivity(View view) {
        Log.d(LOG_TAG, "Vehicles Update clicked!");
        Intent updateVehiclesActivity = new Intent(this, VehicleUpdateActivity.class);
        startActivity(updateVehiclesActivity);
    }

}
