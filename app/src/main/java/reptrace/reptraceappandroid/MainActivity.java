package reptrace.reptraceappandroid;

import android.content.Intent;
import android.content.SharedPreferences;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.CompoundButton;
import android.widget.Switch;



public class MainActivity extends AppCompatActivity {

    private static final String LOG_TAG = MainActivity.class.getSimpleName();

    private Switch switchThemeButton;
    private static final String PREFS_NAME = "prefs";
    private static final String PREF_DARK_THEME = "dark_theme";
    public static MainActivity instance;
    


    @Override
    protected void onCreate(Bundle savedInstanceState) {



        SharedPreferences preferences = getSharedPreferences(PREFS_NAME, MODE_PRIVATE);
        boolean useDarkTheme = preferences.getBoolean(PREF_DARK_THEME, false);

        if(useDarkTheme) {
            setTheme(R.style.AppTheme_Dark);
        }
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_main);

        Switch toggle = (Switch) findViewById(R.id.switchThemeSwitchMain);
        toggle.setVisibility(View.INVISIBLE);
        toggle.setChecked(useDarkTheme);
        toggle.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton view, boolean isChecked) {
                toggleThemeMain(isChecked);
            }
        });

        }



    private void toggleThemeMain(boolean darkTheme) {
        SharedPreferences.Editor editor = getSharedPreferences(PREFS_NAME, MODE_PRIVATE).edit();
        editor.putBoolean(PREF_DARK_THEME, darkTheme);
        editor.apply();

        Intent intent = getIntent();
        finish();

        startActivity(intent);
    }

    public void SettingsActivity (View view) {
        Log.d(LOG_TAG, "Button Settings clicked!");
        Intent appSettingsActivity = new Intent(this, SettingsActivity.class);
        startActivity(appSettingsActivity);

    }

    public void VehiclesActivity (View view) {
        Log.d(LOG_TAG, "Button Vehicles clicked!");
        Intent appVehiclesActivity = new Intent(this, VehiclesActivity.class);
        startActivity(appVehiclesActivity);

    }

    public void RechargeActivity (View view) {
        Log.d(LOG_TAG, "Button Recharge clicked!");
        Intent appRechargeActivity = new Intent(this, RechargeActivity.class);
        startActivity(appRechargeActivity);

    }
    public void EnvironmentZonesActivity (View view) {
        Log.d(LOG_TAG, "Button Environment Zones clicked!");
        Intent appEnvironmentZonesActivity = new Intent(this, EnvironmentZonesActivity.class);
        startActivity(appEnvironmentZonesActivity);

    }



}
