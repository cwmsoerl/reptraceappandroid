package reptrace.reptraceappandroid;

import android.content.Intent;
import android.content.SharedPreferences;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.Switch;
import android.widget.Toast;


import Controllers.DatabaseHelper;
import Models.Vehicles;

public class VehicleUpdateActivity extends AppCompatActivity {

    private static final String LOG_TAG = VehicleUpdateActivity.class.getSimpleName();
    private Switch switchThemeButton;
    private static final String PREFS_NAME = "prefs";
    private static final String PREF_DARK_THEME = "dark_theme";

    DatabaseHelper db;
    EditText updateId;
    EditText updateBrand;
    EditText updateLicensePlate;
    EditText updateEnergyLabel;
    Button updateVehicle;

    Vehicles vehicle;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        SharedPreferences preferences = getSharedPreferences(PREFS_NAME, MODE_PRIVATE);
        boolean useDarkTheme = preferences.getBoolean(PREF_DARK_THEME, false);
        Bundle bundle = this.getIntent().getExtras();
        if (bundle != null) {
            vehicle = bundle.getParcelable("vehicle");
        }

        if(useDarkTheme) {
            setTheme(R.style.AppTheme_Dark);
        }
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_vehicle_update);

        updateBrand = findViewById(R.id.updateBrand);
        updateLicensePlate = findViewById(R.id.updateLicensePlate);
        updateEnergyLabel = findViewById(R.id.updateEnergyLabel);
        if (vehicle != null) {
            updateBrand.setText(vehicle.getvBrand());
            updateLicensePlate.setText(vehicle.getvLicensePlate());
            updateEnergyLabel.setText(vehicle.getvEnergyLabel());
        }


        Switch toggle = (Switch) findViewById(R.id.switchThemeSwitchVehiclesUpdate);
        toggle.setVisibility(View.INVISIBLE);
        toggle.setChecked(useDarkTheme);
        toggle.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton view, boolean isChecked) {
                toggleThemeVehicles(isChecked);
            }
        });


        updateVehicle.setOnClickListener(
                new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        boolean isUpdate = db.updateVehicle(
                                updateId.getText().toString(),
                                updateBrand.getText().toString(),
                                updateLicensePlate.getText().toString(),
                                updateEnergyLabel.getText().toString());

                        if(isUpdate == true)
                            Toast.makeText(VehicleUpdateActivity.this,"Vehicle changes saved",Toast.LENGTH_LONG).show();
                        else
                            Toast.makeText(VehicleUpdateActivity.this,"Vehicle changes not saved ",Toast.LENGTH_LONG).show();
                    }
                }
        );

    }


    private void toggleThemeVehicles(boolean darkTheme) {
        SharedPreferences.Editor editor = getSharedPreferences(PREFS_NAME, MODE_PRIVATE).edit();
        editor.putBoolean(PREF_DARK_THEME, darkTheme);
        editor.apply();

        Intent intent = getIntent();
        finish();

        startActivity(intent);
    }

    public void VehicleActivity(View view) {
        Log.d(LOG_TAG, "Back Button Vehicle Update clicked!");
        Intent vehicleActivity = new Intent(this, VehiclesActivity.class);
        startActivity(vehicleActivity);
    }

}
