package Adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import java.util.ArrayList;
import Models.Vehicles;
import reptrace.reptraceappandroid.R;


public class VehicleAdapter extends BaseAdapter {

    Context context;
    ArrayList<Vehicles> vehiclesArrayList;

    public VehicleAdapter(Context context, ArrayList<Vehicles> vehiclesArrayList){
        this.context = context;
        this.vehiclesArrayList = vehiclesArrayList;
    }

    @Override
    public int getCount() {
        return this.vehiclesArrayList.size();
    }

    @Override
    public Object getItem(int position) {
        return vehiclesArrayList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

            LayoutInflater inflater = (LayoutInflater)context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = inflater.inflate(R.layout.list_item,null);

            TextView vehicleBrandName = (TextView)convertView.findViewById(R.id.vehicleTextView);
            TextView vehicleLicensePlate = (TextView)convertView.findViewById(R.id.licensePlateTextView);
            TextView vehicleEnergyLabel = (TextView)convertView.findViewById(R.id.energyLabelTextView);

            Vehicles vehicles = vehiclesArrayList.get(position);

            vehicleBrandName.setText(vehicles.getvBrand());
            vehicleLicensePlate.setText(vehicles.getvLicensePlate());
            vehicleEnergyLabel.setText(vehicles.getvEnergyLabel());


            return convertView;
    }

}
