package Models;


import android.os.Parcel;
import android.os.Parcelable;

public class Vehicles implements Parcelable {

    int vId;
    String vBrand;
    String vLicensePlate;
    String vEnergyLabel;

    public Vehicles(int vId, String vBrand, String vLicensePlate, String vEnergyLabel) {
        this.vId = vId;
        this.vBrand = vBrand;
        this.vLicensePlate = vLicensePlate;
        this.vEnergyLabel = vEnergyLabel;
    }

    public Vehicles(){ }


    protected Vehicles(Parcel in) {
        vId = in.readInt();
        vBrand = in.readString();
        vLicensePlate = in.readString();
        vEnergyLabel = in.readString();
    }

    public static final Creator<Vehicles> CREATOR = new Creator<Vehicles>() {
        @Override
        public Vehicles createFromParcel(Parcel in) {
            return new Vehicles(in);
        }

        @Override
        public Vehicles[] newArray(int size) {
            return new Vehicles[size];
        }
    };

    public int getvId() {
        return vId;
    }

    public void setvId(int vId) {
        this.vId = vId;
    }

    public String getvBrand() {
        return vBrand;
    }

    public void setvBrand(String vBrand) {
        this.vBrand = vBrand;
    }

    public String getvLicensePlate() {
        return vLicensePlate;
    }

    public void setvLicensePlate(String vLicensePlate) {
        this.vLicensePlate = vLicensePlate;
    }

    public String getvEnergyLabel() {
        return vEnergyLabel;
    }

    public void setvEnergyLabel(String vEnergyLabel) {
        this.vEnergyLabel = vEnergyLabel;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(vId);
        dest.writeString(vBrand);
        dest.writeString(vLicensePlate);
        dest.writeString(vEnergyLabel);
    }
}



