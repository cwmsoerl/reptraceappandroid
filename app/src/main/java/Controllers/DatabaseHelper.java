package Controllers;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import java.util.ArrayList;

import Models.Vehicles;


public class DatabaseHelper extends SQLiteOpenHelper {

    public static final String DB_NAME = "vehicles.db";
    public static final String DB_TABLE_VEHICLES = "vehicles";

    private static final String ID = "vehicles_id";
    private static final String BRAND = "vehicle_brand";
    private static final String LICENSE_PLATE = "licenseplate";
    private static final String ENERGY_LABEL = "energylabel";


    private static final String CREATE_TABLE = "CREATE TABLE " + DB_TABLE_VEHICLES + " ("+
            ID + " INTEGER PRIMARY KEY AUTOINCREMENT, " +
            BRAND + " TEXT, " +
            LICENSE_PLATE + " TEXT," +
            ENERGY_LABEL + " TEXT " + ")";



    public DatabaseHelper (Context context) {
        super(context, DB_NAME, null, 9);

    }

    @Override
    public void onCreate(SQLiteDatabase db) {
    db.execSQL(CREATE_TABLE);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
    db.execSQL("DROP TABLE IF EXISTS vehicles");

    onCreate(db);
    }

    //CRUD

    //CREATE
    public boolean addVehicle (String vehicleName, String licensePlateNr, String energyLabel){
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues contentValues = new ContentValues();
        contentValues.put(BRAND, vehicleName);
        contentValues.put(LICENSE_PLATE, licensePlateNr);
        contentValues.put(ENERGY_LABEL, energyLabel);

        long result = db.insert(DB_TABLE_VEHICLES, null, contentValues);

        db.close();

        return result != -1;
    }

    //READ

    public ArrayList<Vehicles> getAllVehicles() {

        ArrayList<Vehicles> vehiclesArrayList = new ArrayList<>();
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.rawQuery("SELECT * FROM " + DB_TABLE_VEHICLES, null);

        while(cursor.moveToNext()){

            int id = cursor.getInt(0);
            String brand = cursor.getString(1);
            String licensePlate = cursor.getString(2);
            String energyLabel = cursor.getString(3);
            Vehicles vehicles = new Vehicles(id, brand, licensePlate, energyLabel);

            vehiclesArrayList.add(vehicles);
            db.close();
        }

        return vehiclesArrayList;
    }


    //UPDATE
    public boolean updateVehicle(String id, String vehicleName, String licensePlateNr, String energyLabel) {
    SQLiteDatabase db = this.getWritableDatabase();
    ContentValues contentValues = new ContentValues();
        contentValues.put(BRAND, vehicleName);
        contentValues.put(LICENSE_PLATE, licensePlateNr);
        contentValues.put(ENERGY_LABEL, energyLabel);
        db.update(DB_TABLE_VEHICLES, contentValues, "ID = ?",new String[] {id});
        return true;

    }

    //DELETE
    public Integer deleteVehicle(String id){
        SQLiteDatabase db = this.getWritableDatabase();
        db.close();
        return db.delete(DB_TABLE_VEHICLES, "ID = ?", new String[] {id});

    }

}
